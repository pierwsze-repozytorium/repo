# Hej!!

To jest pierwszy paragraf

To jest drugi paragraf

To jest trzeci paragraf

## Modyfikacja tekstu
*To jest kursywa*

**To jest pogrubienie**

~~Ten tekst jest skreślony~~
## Cytat
>To jest cytat

## Lista ponumerowana
1. Jeden
2. Dwa
3. Trzy
    1. podlista
    2. podlista
    3. podlista

## Lista bez numeracji
- jeden
    - jeden
- dwa
    - dwa
- trzy
    - trzy

## Blok kodu
~~~py
x=1
y=2
z=x+y
print(f'Wynik dodawania {x} i {y} wynosi {z}.')
~~~

## Kod w tekście
To jest program, który wypisuje 'Hello world!':  `print('Hello world!')`

## Fajny obrazek
![fajny obrazek](git.jpg)
